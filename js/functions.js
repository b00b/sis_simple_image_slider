$(document).ready(function(){

    var slider;
    var idleTime = 0;

    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 60 sec

    // hide the cursor
    document.body.style.cursor = 'none';

    // parse the img folder for jpg or png (change below acordingly)
    $.ajax({
        url: "img",
        success: function(data){
            $(data).find("a:contains(.png)").each(function(){
                // will loop through
                var images = $(this).attr("href");
                console.log(images);
                $('.bxslider').append('<li><img src="img/' + images + '"></li>');
            });
        },
        complete: function() {
            slider = $('.bxslider').bxSlider({
                mode: 'horizontal',
                moveSlides: 1,
                slideMargin: 0,
                infiniteLoop: true,
                slideWidth: 1920,
                minSlides: 1,
                maxSlides: 1,
                adaptiveHeight: true,
                pager: false,
                speed: 400,
            });
        }
    }).then(doneCallback, failCallback);

    // on load, go to slide 0
    function doneCallback(result) {
        console.log("finished ajax req");
        slider.goToSlide(0);
    }

    // log errors
    function failCallback(result) {
        console.log('Result ' + result);
    }

    function timerIncrement() {
        idleTime = idleTime + 1;
        if (idleTime > 3) { // 1 minutes
            slider.goToSlide(0);
            console.log("idle timeout");
        }
    }

    /* catch events */

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });

    $(this).keypress(function (e) {
        idleTime = 0;
    });
});
