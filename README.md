# SIS - simple image slider 

A web based image slider for jpg or png.
The slider is using this jquery based project:  
https://github.com/stevenwanderski/bxslider-4  
for trasitions 

## install

* install apache2
* clone the repo into /var/www/html
* mkdir img

## prepare images

The slider takes all image sizes and fits them to the screen, but for best performance it is good practize to resize them to the screen resolution.
e.g. FullHD

```
## convert images from jpg to png and resize on the fly
cd imagefolder
find . -name "*.jpg" -exec mogrify -resize 1920x -format png -quality 95% {} \;
```

Place the resized images in the img folder. You don´t need to change any code. The img folder gets parsed automatically for imagefiles.

## configuration

In the functions.js file in js directory you can change setting based on the documentation found here:  
https://github.com/stevenwanderski/bxslider-4

The slider has an idle timer that moves to slide 0 after 3 minutes of idletime  

