#!/bin/bash

export DISLPAY=:0

killall chromium-browser

chromium-browser --kiosk --incognito --disable-pinch http://localhost &
